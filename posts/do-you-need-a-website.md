---
title: Do you need a website?
tags: strategy
---

We often turn down client requests for websites. Our time and energy is valuble, and we don’t anyone to spend any longer than they need to staring at a computer screen. So in response, we offer strategic alternatives that meet their needs without wasting time and energy on a project they’e not a ready for.

Here’s a scenario:

A potential client begins to explain how they’ve been looking for someone they can trust to build their website. They need the website to have this many pages, this many features, look like this, behave like that and so on.

We’ll have a listen and respond with something like “Great! Why will this website help your business?”

Then our potential client may say something like, “People need to find out when we’re open.” Or maybe they’ll say, “We’re not getting of enough customers for our events.” Or maybe people will say “We need a website to appear more established.”

Notice any issues with this approach?

When people want to see when a business is open, they’ll most likely check Google Maps or FaceBook. If people aren’t getting enough customers, then a marketing plan can help create more interest. A website won’t make you appear more established especially if it has no content. Our current infatration with technology leads many to think any problem can be solved with a technical solution. A website won’t save your business, though it can enhance it.

We understand many people are looking for shortcuts or some magic pill to solve all their problems. Though it takes patience, consistenccy, and hardwork to create a brand or business. If you’re not established, a website could help though it could be a waste without a marketing strategy to back it up. Why would someone visit a random website for something they could quickly find through online search? Why would someone go to your website rather than keep scrolling through social media that continually feeds them exactly what they want?