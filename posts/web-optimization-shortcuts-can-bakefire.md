---
layout: base.njk
title: Web optimization shortcuts can backfire
tags: optimization
---

I worked on a tricky WordPress where none of the [Ninja Forms](https://ninjaforms.com/) were appearing on the site. After spending some time looking at the web server logs and the WordPress error log, nothing showed up. Then I checked the JavaScript console and finally found some wierdness. Ofcourse it was a random error without much of a way to trace it back to the source.

Experience has taught me though. These cryptic errors can be caused by the optimization found in a lot of the perfomance plugins and services promoted with WordPress. This site was running [Siteground Optimizer](https://wordpress.org/plugins/sg-cachepress/) so I gave those settings a few tests but saw no improvement. Clearing the page builder cache and server cache brought no joy either.

Ahh...[CloudfFlare](https://www.cloudflare.com/) was in play.

This error was caused by CloudFlare and the minify JavaScript feature. Turning this feature off brought form display back.

Its really tempting to think you can flip a switch and speed up a site. Yea this works sometimes, however, making a website run well often isn't so simple especially with sites that have accumulated years of content. As for the cause of this issue, my best guess is that Ninja Forms changed the way JavaScript assets were called up. Optimization plugins often have checklist and progress guides that encourse users to turn on all the features without really checking for compatibility. On the other hand, lots of other folks have [issues with bugs after a Ninja Form udpate](https://wordpress.org/support/topic/ninja-forms-not-showing-on-chrome-or-safari/) and the plugin has had a [major security flub](https://www.searchenginejournal.com/wordpress-ninja-forms-vulnerability-exposes-over-a-million-sites/420726/). So this could be more the responsibility of the form plugin author rather CloudFlare...