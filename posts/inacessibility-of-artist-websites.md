---
layout: base.njk
tags: artists
title: The inaccessibility of Artist websites
---

Its funny how the aloofness of modern art makes an appearance in web design.

Content
sense of detachment makes forming connection difficult
large globs of text are difficult to navigate onscreen

Bad Typography
* poor use of whitespace
* small line height
* small font sizes
* creative text effects that distract rather than communicate

Imagery
* large, un optimized images that drag down page speed
* images often lack captions and context
* lack of an artist statement

Optimization
* Huge image, heavy pages 

Summary
How does my website reach the people who can help me most? How does it reach to people I want to help most?
How can work be displayed without relying on lots of high res imagery?
Does detachment and lack of connection hinder goals?
How can I balnce my website as an aesthic/artistic medium and a communication tool?