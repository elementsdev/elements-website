---
layout: post.njk
tags: artists
title: Preparing to build a website
---

Before creating a website there are some important things you can do to prepare. Many forego simple and important steps in website planning since its easy to jump into the web with the plethora of site builders coming to market. The success of a website is intimately tied to the clarity of vision and goals it seeks to promote.

Lay Down a Business Plan
------------------------

In essence a website serves as a public showcase of your[ business plans](http://www.bplans.com/sample_business_plans.php). Several notable parts of a business plan that contribute directly to a website are the mission and vision statement, target audience, market analysis, company profile, provided services, and pricing structure. Going into a website build with strategic plans saves time and reduces costs by speeding up content creation while ensuring unified progress toward a chosen destination.

Think About Branding and Visual Identity
----------------------------------------

Logos, business cards, letterhead, colors, and typography all contribute to the [visual identity](http://justcreative.com/2010/04/06/branding-identity-logo-design-explained/) of a website. Business owners with branding assets in place can breeze through the web design process since half of the work of crafting an online presence has been done. So remember you have the option of hiring an designer to craft the visual identity for your organization and then adding a developer to the team to create the website.

I happily take on basic branding projects for my clients which often includes a basic logo, typographic selection, color palette, imagery, business cards and other stationary. For larger or highly specialized projects, I call upon brand designers and illustrators to bring specialized expertise to the table.

Check out [Inspiration Grid](http://theinspirationgrid.com/category/identity/) and [Stationary Overdose](http://www.stationeryoverdose.com/) for some great examples of branding.

Draw Up a Sitemap
-----------------

A [sitemap](http://boxesandarrows.com/the-lazy-ias-guide-to-making-sitemaps/) is a simple outline for your website. At minimum most websites include a Home, About, and Contact page. E-commerce sites may add a  Store page and sites that inform and education may include a Blog. Increase the chances of your website's success by doing some preliminary research to find websites of competitors and peers. Study their pages and use the findings and inspiration for your own site outline. As a result you'll have the advantage of going to a web designer with a sitemap in hand which makes pricing quotes more accurate and your expectations clear.

Don't Buy Templates or Themes in Advance
----------------------------------------

Often clients come to me with product X and ask me to either customize or use it to build a complete site. Unfortunately, most of the time I regretfully inform them that the product they chose is poor quality and will incur high maintenance and development costs. There are many website themes and plugins that have great, attractive marketing and a surprisingly low price but [lack reliability, performance, and ease of use](http://slobodanmanic.com/321/buying-themeforest-insane/). Many don't find out this unpleasant truth until after the product fails to meet their needs two to three months into use.

Leave template and plugin selection up to your developer. You'll benefit from their experience and expertise and they will be working with tools they know and understand.

Prepare a Budget
----------------

Decide on a rough budget for your website that includes both time and money. Along with the cost of hiring a developer to prepare and build the web site, remember to plan for recurring costs which may include any of the following:

-   domain registration -- $12 to $80 per year
-   hosting -- $5 to $100 per month
-   software renewals -- $50 or more per year

Other costs may include:

-   website maintenance and upkeep -- $150 or more per month
-   web apps -- $10 to $60 per subscription
-   premium plugins -- $50 or more per plugin
-   premium theme -- $50 to $150
-   additional professionals (photographers, illustrators, animators) -- $300 +

An experienced creative professional should be able to work within your budget, prepare common costs and estimates, and help ensure that your project is profitable. Additionally, if you plan to carry out updates and edits yourself then be sure to allocate at least four hours per week to website maintenance.

Make Notes on Peer and Competitor Websites
------------------------------------------

Take the market analysis of your business plan to the web by making two lists.

-   the websites of peers and competitors
-   the websites that you enjoy using and would best fit your vision

There may be overlap on either of these lists and thats OK. Taking some time to build a visual vocabulary will help to hone in on a design ethos and reveal features best suited for your needs. Some great places to look for inspiration are [Site Inspire](https://www.siteinspire.com/) and [Awwwards](http://www.awwwards.com/).

Take Your Time and Do it Right
------------------------------

I hope you've been motivated to think through the purpose and goals behind your website. By doing so, you'll be prepared to venture toward building your own site or have ready made plans for your web designer. Taking the time to do things right now will pay off much appreciated dividends in the long run.